# ngocdung_stringee_flutter

This project using Stringee's Flutter plugin and Ngocdung's API to get token & userId for calling app-to-phone.

Please overall reading [Stringee Call API SDK](https://developer.stringee.com/docs/call-api-getting-started) document to know how android and iOS navtive's workflow, before you read below content.


### Create new folder, download and locate plugin

* Create new folder
* Download plugin [here](https://github.com/stringeecom/stringee_flutter_plugin)
* Located plugin like below
<img src="https://i.imgur.com/5cCixrY.png" alt=" " width="40%"/>


### Relocate Flutter project folder

* Move Flutter project "example" to outside look like below.


<img src="https://i.imgur.com/jEZKnwo.png" alt=" " width="40%"/>

* So that, you can use the plugin like a **package**. The Flutter project **"example"** can depend on it. 


>  For more details, please read offical document about [Dependencies on unpublished packages](https://flutter.dev/docs/development/packages-and-plugins/using-packages#dependencies-on-unpublished-packages)


### Update Stringee package's path

* Open project **example**. You will see errors look like below, because plugin's path was wrong. To fix it, follow next steps.

<img src="https://i.imgur.com/o6gpTYs.png" alt=" " width="60%"/>


* Open file **pubspec.yaml**
* Update **stringee_flutter_plugin package's path** from `../` to `../stringee_flutter_plugin`

<img src="https://i.imgur.com/Y3sktrk.png" alt=" " width="40%"/>


*  Finally, run command `flutter pub get`. Errors was gone.


### Connect to Stringee's server

Next, we will connect to **Stringee Server**. You must do this before you can make or answer a call.

To connect to server, you must have access token.











