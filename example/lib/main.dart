import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:stringee_flutter_plugin/stringee_flutter_plugin.dart';
import 'package:stringee_flutter_plugin_example/info_client_response.dart';

import 'Call.dart';

var tokenTest =
    "eyJjdHkiOiJzdHJpbmdlZS1hcGk7dj0xIiwidHlwIjoiSldUIiwiYWxnIjoiSFMyNTYifQ.eyJqdGkiOiJTSzJlbmVodlR3YmxmZXlROWpGOUVld3Z5ZXdxdlBPcW12LTE1ODM4OTc0MDAiLCJpc3MiOiJTSzJlbmVodlR3YmxmZXlROWpGOUVld3Z5ZXdxdlBPcW12IiwiZXhwIjoxNTg2NDg5NDAwLCJ1c2VySWQiOiJidXNpbmVzcy5udjEiLCJpY2NfYXBpIjp0cnVlfQ.6jk44h-iqQdpX0p2BB9Mq0Cixn3t9yjUbbOxBLGsGXk";
var phoneTest = "842471008856";

var client = StringeeClient();

String strUserId = "";

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(title: "OneToOneCallSample", home: new MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  String myUserId = 'Not connected...';

  InfoClientResponse clientData;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // Lắng nghe sự kiện của StringeeClient(kết nối, cuộc gọi đến...)
    client.eventStreamController.stream.listen((event) {
      Map<dynamic, dynamic> map = event;
      StringeeClientEventType eventType = map['eventType'];

      print(eventType.toString());

      switch (eventType) {
        case StringeeClientEventType.DidConnect:
          handleDidConnectEvent();
          break;
        case StringeeClientEventType.DidDisconnect:
          handleDiddisconnectEvent();
          break;
        case StringeeClientEventType.DidFailWithError:
          break;
        case StringeeClientEventType.RequestAccessToken:
          break;
        case StringeeClientEventType.DidReceiveCustomMessage:
          break;
        case StringeeClientEventType.IncomingCall:
          StringeeCall call = map['body'];
          handleIncomingCallEvent(call);
          break;
        default:
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget topText = new Container(
      padding: EdgeInsets.only(left: 10.0, top: 10.0),
      child: new Text(
        'Connected as: $myUserId',
        style: new TextStyle(
          color: Colors.black,
          fontSize: 20.0,
        ),
      ),
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("OneToOneCallSample"),
        backgroundColor: Colors.indigo[600],
      ),
      body: new Stack(
        children: <Widget>[
          topText,
          Form(
//      key: _formKey,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.all(20.0),
                  child: new TextField(
                    onChanged: (String value) {
                      _changeText(value);
                    },
                    decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.red),
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new RaisedButton(
                          color: Colors.grey[300],
                          textColor: Colors.black,
                          padding: EdgeInsets.only(left: 40.0, right: 40.0),
                          onPressed: _voiceCallTapped,
                          child: Text('CALL'),
                        ),
                      ]),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //region Handle Client Event
  void handleDidConnectEvent() {
    setState(() {
      myUserId = client.userId;
    });

    if (strUserId.isEmpty || !client.hasConnected) {
      return;
    }

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Call(
                fromUserId: clientData.data.phone,
//                fromUserId: phoneTest,
                toUserId: strUserId,
                showIncomingUi: false
              )),
    );
  }

  void handleDiddisconnectEvent() {
    setState(() {
      myUserId = 'Not connected...';
    });
  }

  void handleIncomingCallEvent(StringeeCall call) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Call(
              fromUserId: call.from,
              toUserId: call.to,
              showIncomingUi: true,
              incomingCall: call)),
    );
  }

  void _changeText(String val) {
    setState(() {
      strUserId = val;
    });
  }

  void _voiceCallTapped() {
    if (strUserId.isEmpty) {
      return;
    }

    if (client.hasConnected) {
      client.disconnect();
    }

    getAccessToken(phoneNumber: strUserId).then((response) {
      clientData = response;
      client.connect(response.data.token);
//      client.connect(tokenTest);
    });
  }
}

Future<InfoClientResponse> getAccessToken({String phoneNumber}) async {
  var options = BaseOptions();

  options..headers = {'Content-Type': 'application/json; charset=utf-8'};
  options..headers.putIfAbsent('authorization', () => "5B84BF5EC15CDBAF7");

  Dio dio = Dio(options);

  var result = await dio.get(
      "https://apicall.ngocdunggroup.com.vn/call/info-client-platform?" +
          "product=APP&phoneNumber=$phoneNumber" +
          "&platform=APP" +
          "&token=3fb844e8a1b5b1ea721099d9898f279ce9e8ac9a6e2b34846168bd18dce78504");
  var responseData = InfoClientResponse.fromJson(result.data);
  print(responseData.toString());
  return responseData;
}
