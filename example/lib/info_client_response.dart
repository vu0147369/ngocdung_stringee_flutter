class InfoClientResponse {
  int status;
  String message;
  int errorCode;
  Data data;

  InfoClientResponse({this.status, this.message, this.errorCode, this.data});

  InfoClientResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    errorCode = json['error_code'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['error_code'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return 'InfoClientResponse{status: $status, message: $message, errorCode: $errorCode, data: $data}';
  }


}

class Data {
  String token;
  String phone;

  Data({this.token, this.phone});

  Data.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['phone'] = this.phone;
    return data;
  }

  @override
  String toString() {
    return 'Data{token: $token, phone: $phone}';
  }

}

